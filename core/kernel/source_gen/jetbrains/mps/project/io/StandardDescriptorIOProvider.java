package jetbrains.mps.project.io;

/*Generated by MPS */

import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import jetbrains.mps.project.structure.modules.SolutionDescriptor;
import jetbrains.mps.project.structure.modules.LanguageDescriptor;
import jetbrains.mps.project.structure.modules.DevkitDescriptor;
import jetbrains.mps.vfs.IFile;
import jetbrains.mps.util.MacroHelper;
import jetbrains.mps.util.MacrosFactory;
import org.jdom.Document;
import jetbrains.mps.util.JDOMUtil;
import org.jdom.Element;
import jetbrains.mps.project.persistence.SolutionDescriptorPersistence;
import jetbrains.mps.project.persistence.ModuleReadException;
import jetbrains.mps.project.persistence.ModuleDescriptorPersistence;
import org.apache.log4j.Level;
import jetbrains.mps.project.persistence.LanguageDescriptorPersistence;
import jetbrains.mps.project.structure.modules.GeneratorDescriptor;
import java.io.IOException;
import jetbrains.mps.project.persistence.DevkitDescriptorPersistence;

public class StandardDescriptorIOProvider implements DescriptorIOProvider {
  private static final Logger LOG = LogManager.getLogger(StandardDescriptorIOProvider.class);
  private static final StandardDescriptorIOProvider.SolutionDescriptorIO SOLUTION = new StandardDescriptorIOProvider.SolutionDescriptorIO();
  private static final StandardDescriptorIOProvider.LanguageDescriptorIO LANGUAGE = new StandardDescriptorIOProvider.LanguageDescriptorIO();
  private static final StandardDescriptorIOProvider.DevkitDescriptorIO DEVKIT = new StandardDescriptorIOProvider.DevkitDescriptorIO();
  /*package*/ StandardDescriptorIOProvider() {
  }
  @Override
  public DescriptorIO<SolutionDescriptor> solutionDescriptorIO() {
    return SOLUTION;
  }
  @Override
  public DescriptorIO<LanguageDescriptor> languageDescriptorIO() {
    return LANGUAGE;
  }
  @Override
  public DescriptorIO<DevkitDescriptor> devkitDescriptorIO() {
    return DEVKIT;
  }
  public static class SolutionDescriptorIO implements DescriptorIO<SolutionDescriptor> {
    public SolutionDescriptorIO() {
    }
    @Override
    public SolutionDescriptor readFromFile(IFile file) throws DescriptorIOException {
      SolutionDescriptor descriptor;
      try {
        MacroHelper macroHelper = MacrosFactory.forModuleFile(file);
        Document document = JDOMUtil.loadDocument(file);
        Element rootElement = document.getRootElement();
        descriptor = new SolutionDescriptorPersistence(macroHelper).load(rootElement);
      } catch (Exception ex) {
        descriptor = new SolutionDescriptor();
        ModuleReadException mre = (ex instanceof ModuleReadException ? ((ModuleReadException) ex) : new ModuleReadException(ex));
        ModuleDescriptorPersistence.loadBrokenModule(descriptor, file, mre);
      }
      ModuleDescriptorPersistence.setTimestamp(descriptor, file);
      return descriptor;
    }

    @Override
    public void writeToFile(SolutionDescriptor sd, IFile file) {
      if (file.isReadOnly()) {
        // XXX why on earth do we check for read-only here? why not in a caller code, where one could have reacted reasonably? 
        if (LOG.isEnabledFor(Level.ERROR)) {
          LOG.error("Can't save " + file.getPath());
        }
        return;
      }

      try {
        MacroHelper macroHelper = MacrosFactory.forModuleFile(file);
        Element result = new SolutionDescriptorPersistence(macroHelper).save(sd);
        JDOMUtil.writeDocument(new Document(result), file);
      } catch (Exception e) {
        if (LOG.isEnabledFor(Level.ERROR)) {
          LOG.error("Failed to serialize solution descriptor", e);
        }
      }

      ModuleDescriptorPersistence.setTimestamp(sd, file);
    }
    @Override
    public void writeToXml(SolutionDescriptor sd, Element element, IFile anchorFile) {
      throw new UnsupportedOperationException();
    }
    @Override
    public SolutionDescriptor readFromXml(Element element, IFile anchorFile) {
      throw new UnsupportedOperationException();
    }
  }

  public static class LanguageDescriptorIO implements DescriptorIO<LanguageDescriptor> {
    public LanguageDescriptorIO() {
    }
    @Override
    public LanguageDescriptor readFromFile(IFile file) throws DescriptorIOException {
      LanguageDescriptor descriptor;

      try {
        MacroHelper macroHelper = MacrosFactory.forModuleFile(file);
        Document document = JDOMUtil.loadDocument(file);
        Element languageElement = document.getRootElement();
        descriptor = new LanguageDescriptorPersistence(macroHelper).load(languageElement);
      } catch (Exception ex) {
        descriptor = new LanguageDescriptor();
        ModuleReadException mre = (ex instanceof ModuleReadException ? ((ModuleReadException) ex) : new ModuleReadException(ex));
        ModuleDescriptorPersistence.loadBrokenModule(descriptor, file, mre);
      }

      ModuleDescriptorPersistence.setTimestamp(descriptor, file);
      for (GeneratorDescriptor gd : descriptor.getGenerators()) {
        ModuleDescriptorPersistence.setTimestamp(gd, file);
      }

      return descriptor;
    }

    @Override
    public void writeToFile(LanguageDescriptor ld, IFile file) {
      if (file.isReadOnly()) {
        if (LOG.isEnabledFor(Level.ERROR)) {
          LOG.error("Cant't save " + file.getPath());
        }
        return;
      }
      try {
        MacroHelper macroHelper = MacrosFactory.forModuleFile(file);
        Element element = new LanguageDescriptorPersistence(macroHelper).save(ld);
        Document doc = new Document(element);
        JDOMUtil.writeDocument(doc, file);
        // XXX is it always a need to refresh timestamp in the descriptor? What if serialize it into a copy file 
        ModuleDescriptorPersistence.setTimestamp(ld, file);
      } catch (IOException ex) {
        if (LOG.isEnabledFor(Level.ERROR)) {
          LOG.error("Failed to serialize language descriptor", ex);
        }
      }
    }

    @Override
    public void writeToXml(LanguageDescriptor ld, Element element, IFile anchorFile) {
      throw new UnsupportedOperationException();
    }
    @Override
    public LanguageDescriptor readFromXml(Element element, IFile anchorFile) {
      throw new UnsupportedOperationException();
    }
  }

  public static class DevkitDescriptorIO implements DescriptorIO<DevkitDescriptor> {
    public DevkitDescriptorIO() {
    }
    @Override
    public DevkitDescriptor readFromFile(IFile file) throws DescriptorIOException {
      DevkitDescriptor descriptor;
      try {
        Document document = JDOMUtil.loadDocument(file);
        descriptor = new DevkitDescriptorPersistence().load(document.getRootElement());
      } catch (Exception ex) {
        descriptor = new DevkitDescriptor();
        ModuleReadException mre = (ex instanceof ModuleReadException ? ((ModuleReadException) ex) : new ModuleReadException(ex));
        ModuleDescriptorPersistence.loadBrokenModule(descriptor, file, mre);
      }

      ModuleDescriptorPersistence.setTimestamp(descriptor, file);
      return descriptor;
    }

    @Override
    public void writeToFile(DevkitDescriptor dd, IFile file) {
      try {
        Element root = new DevkitDescriptorPersistence().save(dd);
        JDOMUtil.writeDocument(new Document(root), file);
      } catch (Exception e) {
        if (LOG.isEnabledFor(Level.ERROR)) {
          LOG.error("Failed to serialize devkit descriptor", e);
        }
      }
      ModuleDescriptorPersistence.setTimestamp(dd, file);

    }

    @Override
    public DevkitDescriptor readFromXml(Element element, IFile anchorFile) {
      throw new UnsupportedOperationException();
    }
    @Override
    public void writeToXml(DevkitDescriptor t, Element element, IFile anchorFile) {
      throw new UnsupportedOperationException();
    }
  }
}
