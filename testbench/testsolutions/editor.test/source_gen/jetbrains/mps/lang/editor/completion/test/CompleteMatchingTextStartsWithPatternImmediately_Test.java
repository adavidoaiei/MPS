package jetbrains.mps.lang.editor.completion.test;

/*Generated by MPS */

import jetbrains.mps.MPSLaunch;
import jetbrains.mps.lang.test.runtime.BaseTransformationTest;
import org.junit.ClassRule;
import jetbrains.mps.lang.test.runtime.TestParametersCache;
import org.junit.Test;
import jetbrains.mps.lang.test.runtime.BaseEditorTestBody;

@MPSLaunch
public class CompleteMatchingTextStartsWithPatternImmediately_Test extends BaseTransformationTest {
  @ClassRule
  public static final TestParametersCache ourParamCache = new TestParametersCache(CompleteMatchingTextStartsWithPatternImmediately_Test.class, "${mps_home}", "r:f27d9626-8ef5-4cba-bce0-6aa6369f05ff(jetbrains.mps.lang.editor.completion.test)", false);

  public CompleteMatchingTextStartsWithPatternImmediately_Test() {
    super(ourParamCache);
  }

  @Test
  public void test_CompleteMatchingTextStartsWithPatternImmediately() throws Throwable {
    runTest("jetbrains.mps.lang.editor.completion.test.CompleteMatchingTextStartsWithPatternImmediately_Test$TestBody", "testMethod", false);
  }

  @MPSLaunch
  public static class TestBody extends BaseEditorTestBody {
    @Override
    public void testMethodImpl() throws Exception {
      initEditorComponent("2070818307959506191", "2070818307959506193");
      typeString("ful");
      invokeAction("jetbrains.mps.ide.editor.actions.Complete_Action");
    }
  }
}
